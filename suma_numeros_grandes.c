#include <stdio.h>
#include <string.h>
#include <stdlib.h>


char *invertir(char cadena[]) ; 


//funcion para invertir una cadena
char *invertir(char cadena[]) {
  int longitud = strlen(cadena);
  char temporal;
  for (int izquierda = 0, derecha = longitud - 1; izquierda < (longitud / 2);
       izquierda++, derecha--) {
    temporal = cadena[izquierda];
    cadena[izquierda] = cadena[derecha];
    cadena[derecha] = temporal;
  }
  return cadena;
}

int main(void){

    int carry=0,n1,n2,re,i,j,long1,long2;
    //Solicitando longitud en digitos de los numeros
    printf("\nDefina digitos del numero 1: ");
    scanf("%d",&long1);
    printf("\nDefina digitos del numero 2: ");
    scanf("%d",&long2);
    
    while(getchar() != '\n')
    ;
    //verificamos la longitud mas grande para definir las cadenas
     int lo;
     if(long1<long2){
            lo =long2;
        }else{
            lo =long1;
        }
    char num1[long1+1],num2[long2+1],aux[lo+1],res[lo+1];
    //solicitamos y almacenamos el numero 1 en una cadena
    printf("\nIngrese el numero 1: ");
    fgets(num1,long1+1,stdin);
    printf("\n#1 = ");
    for(i=0;i<=long1;i++){
    printf("%c",num1[i]);
    }
    //pausa 
    while(getchar() != '\n')
    ;
    //solicitamos y almacenamos el numero 2 en una cadena
    printf("\nIngrese el numero 2: ");
    fgets(num2,long2+1,stdin);
    printf("\n#2 =  ");
    for(i=0;i<=long2;i++){
        printf("%c",num2[i]);
    }
    
    while(getchar() != '\n')
    ;
    //inicializamos las variables necesarias
    strcpy(res,"");
    if(long1<long2){
        i=long2;
        j=long1;
        strcpy (aux,num1);
        strcpy (num1,num2);
        strcpy (num2,aux); 
    }else{
        i=long1;
        j=long2;
    }

    i--;
    j--;
    //abrimos bloque do while 
do{
    //inicializando variables de suma de un digito
    //printf("\niteracion %d",i);
    n1 = num1[i] - '0';
    //printf("  n1 = %d",n1);
   //comprobando digitos del numero mas pequeño
    if (j>=0){
        n2 = num2[j] - '0';
    }else{ n2=0; }
    //printf("  n2 = %d",n2);
    re=n1+n2+carry;
    //printf("  resultado  = %d",re);
    //comprobacion de acarreo
    if(re >= 10 ){
        strcpy(aux,"");
        sprintf(aux,"%d",re);
        carry = aux[0] - '0';
        re = aux[1] - '0';
        sprintf(aux,"%d",re);
        strcat(res,aux);
    }else{
        carry = 0;
        sprintf(aux,"%d",re);
        strcat(res,aux);
    }
    //decremento de las variables del ciclo

    i--;
    j--;
}while(i>=0);
    
printf("\nRes =  ");
    strcpy(aux,invertir(res));
    strcpy(res,aux);
        for(int x=0;x<=long2;x++){
            printf("%c",res[x]);
        }
        printf("\n");
        while(getchar() != '\n')
    ;
}


